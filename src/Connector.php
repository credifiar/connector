<?php
namespace Credifiar\Connector;

use Credifiar\Connector\IConnector;
use Credifiar\Connector\Reader;
use GuzzleHttp\Client;
use GuzzleHttp\Command\Guzzle\Description;
use GuzzleHttp\Command\Guzzle\GuzzleClient;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Command\Exception\CommandException;
use Credifiar\Connector\Exceptions\ConnectorException;

class Connector implements IConnector
{

    private $client;

    private $guzzleClient;

    private $description;

    private $response;

    function __construct()
    {
        $this->client = new Client(array(
            'headers' => array(
                'Accept' => 'application/json'
            )
        ));
    }

    private function setDescription($description)
    {
        $reader = new Reader($description);
        $this->description = new Description($reader->getData());
    }

    public function init($api)
    {
        $this->setDescription($api);
        $this->guzzleClient = new GuzzleClient($this->client, $this->description);
    }

    public function getParams($method)
    {
        return $this->guzzleClient->getDescription()
            ->getOperation($method)
            ->getParams();
    }

    public function execute($method, array $params = array())
    {
        try {
            $command = $this->guzzleClient->getCommand($method, $params);
            $this->response = $this->guzzleClient->execute($command);
            if ($this->response instanceof \GuzzleHttp\Command\Result) {
                return new Transformer($this->response);
            } else {
                throw new \Exception("Can not connect with 'back'.");
            }
        } catch (CommandException $e) {
            throw new ConnectorException($e);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}