<?php
namespace Credifiar\Connector;

use Illuminate\Support\Facades\File;

class Reader
{

    const dfPath = __DIR__ . '/descriptions';

    private $folder = array();

    private $file;

    private $data = array();

    function __construct($description)
    {
        $this->read($description);
    }

    private function loadFolder()
    {
        try {
            $this->folder = File::allFiles(self::dfPath);
        } catch (InvalidArgumentException $e) {
            throw $e;
        }
    }

    private function search($description)
    {
        foreach ($this->folder as $file) {
            $extension = '.' . $file->getExtension();
            $nameFile = $file->getBasename($extension);
            if (strtolower($description) == strtolower($nameFile)) {
                return $file;
            }
        }
        return false;
    }

    private function read($description)
    {
        $this->loadFolder();
        if (! $this->file = $this->search($description)) {
            throw new \Exception('Description not found');
        }
        
        switch ($this->file->getExtension()) {
            case 'php':
                $this->data = include $this->file->getRealPath();
                break;
            
            default:
                break;
        }
    }

    public function getData()
    {
        return $this->data;
    }

    public function get($key)
    {
        return array_get($this->data, $key);
    }
}