<?php
namespace Credifiar\Connector;

interface IConnector
{

    /**
     * Inicia el connector
     *
     * @param string $api            
     */
    public function init($api);

    /**
     * Obtiene los parametros de un metodo de la descripcion inicializada anteriomente.
     * 
     * @param string $method            
     * @return array
     */
    public function getParams($method);

    /**
     * Ejecuta un metodo de una descripción.
     *
     * @param string $method            
     * @param array $params            
     */
    public function execute($method, array $params = array());
}