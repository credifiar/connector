<?php
namespace Credifiar\Connector\Providers;

use Illuminate\Support\ServiceProvider;
use Credifiar\Connector\Connector;
use Credifiar\Connector\IConnector;

class ConnectorServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/connector.php' => config_path('connector.php')
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(IConnector::class, Connector::class);
    }
}
