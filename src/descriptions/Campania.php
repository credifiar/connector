<?php
return [
    'baseUri' => config('connector.back_uri'),
    'operations' => [
        'getByFecIni' => [
            'summary' => "Obtiene todas las campanias activas credifiar, por fecha de creacion.",
            'httpMethod' => "GET",
            'uri' => "/campanias/",
            'responseModel' => "defaultOutput",
            'parameters' => [
                'desde' => [
                    'location' => "query"
                ],
                'hasta' => [
                    'location' => "query"
                ]
            ]
        ],
        'getByID' => [
            'summary' => "Obtiene una campania por id.",
            'httpMethod' => "GET",
            'uri' => "/campanias/{id}",
            'responseModel' => "defaultOutput",
            'parameters' => [
                'id' => [
                    'location' => "uri",
                    'required' => true
                ]
            ]
        ],
        'store' => [
            'summary' => "Guarda una nueva campania en la bd.",
            'httpMethod' => "POST",
            'uri' => "/campanias/guardar",
            'responseModel' => "defaultOutput",
            'parameters' => [
                'campanias' => [
                    'location' => "json",
                    'type' => "array"
                ]
            ]
        ],
        'update' => [
            'summary' => "Guarda una nueva campania en la bd.",
            'httpMethod' => "POST",
            'uri' => "/campanias/actualizar",
            'responseModel' => "defaultOutput",
            'parameters' => [
                'campanias' => [
                    'location' => "json",
                    'type' => "array"
                ]
            ]
        ],
        'delete' => [
            'summary' => "Da de baja una campania en la bd.",
            'httpMethod' => "DELETE",
            'uri' => "/campanias/borrar/{id}",
            'responseModel' => "defaultOutput",
            'parameters' => [
                'id' => [
                    'location' => "uri",
                    'required' => true
                ]
            ]
        ]
    ],
    'models' => [
        'defaultOutput' => [
            'type' => "object",
            'additionalProperties' => [
                'location' => 'json'
            ]
        ]
    ]
];
?>