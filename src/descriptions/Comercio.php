<?php
return [
    'baseUri' => config('connector.back_uri'),
    'operations' => [
        'getAll' => [
            'httpMethod' => "GET",
            'uri' => "/consumo/comercios",
            'responseModel' => "getAllOutput"
        ]
    ],
    'models' => [
        'getAllOutput' => [
            'type' => "object",
            'additionalProperties' => [
                'location' => 'json'
            ]
        ]
    ]
];
?>