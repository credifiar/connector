<?php
return [
    'baseUri' => config('connector.back_uri'),
    'operations' => [
        'getAll' => [
            'summary' => "Obtiene todos los metodos de contacto que existen.",
            'httpMethod' => "GET",
            'uri' => "/campanias/metodoscontacto/",
            'responseModel' => "getAllOutput"
        ]
    ],
    'models' => [
        'getAllOutput' => [
            'type' => "object",
            'additionalProperties' => [
                'location' => 'json'
            ]
        ]
    ]
];
?>