<?php
return [
    'baseUri' => config('connector.back_uri'),
    'operations' => [
        'getAll' => [
            'summary' => "Obtiene todos los modelos de carta que existen.",
            'httpMethod' => "GET",
            'uri' => "campanias/cartas/modelos/",
            'responseModel' => "defaultOutput"
        ]
    ],
    'models' => [
        'defaultOutput' => [
            'type' => "object",
            'additionalProperties' => [
                'location' => 'json'
            ]
        ]
    ]
];
?>