<?php
return [
    'baseUri' => config('connector.back_uri'),
    'operations' => [
        'getByDocumentos' => [
            'summary' => "Obtiene uno o mas clientes por documento.",
            'httpMethod' => "POST",
            'uri' => "campanias/contactos/documentos",
            'responseModel' => "defaultOutput",
            'parameters' => [
                'documentos' => [
                    'location' => "json",
                    'type' => 'array'
                ]
            ]
        ]
    ],
    'models' => [
        'defaultOutput' => [
            'type' => "object",
            'additionalProperties' => [
                'location' => 'json'
            ]
        ]
    ]
];
?>