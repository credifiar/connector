<?php
return [
    'baseUri' => config('connector.back_uri'),
    'operations' => [
        'getAll' => [
            'summary' => "Obtiene todos las remitentes.",
            'httpMethod' => "GET",
            'uri' => "campanias/cartas/remitentes/",
            'responseModel' => "defaultOutput"
        ]
    ],
    'models' => [
        'defaultOutput' => [
            'type' => "object",
            'additionalProperties' => [
                'location' => 'json'
            ]
        ]
    ]
];
?>