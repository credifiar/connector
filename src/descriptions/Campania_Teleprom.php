<?php
return [
    'baseUri' => config('connector.back_uri'),
    'operations' => [
        'getByFecIni' => [
            'summary' => "Obtiene todas las campanias activas en teleprom que no existan ya en campanias de credifiar, por fecha de inicio.",
            'httpMethod' => "GET",
            'uri' => "/campanias/teleprom/activas",
            'responseModel' => "defaultOutput",
            'parameters' => [
                'desde' => [
                    'location' => "query"
                ],
                'hasta' => [
                    'location' => "query"
                ]
            ]
        ]
    ],
    'models' => [
        'defaultOutput' => [
            'type' => "object",
            'additionalProperties' => [
                'location' => 'json'
            ]
        ]
    ]
];
?>