<?php
return [
    'baseUri' => config('connector.back_uri'),
    'operations' => [
        'getByDocumento' => [
            'summary' => "Obtiene uno o mas clientes por documento.",
            'httpMethod' => "GET",
            'uri' => "/clientes/{documento}",
            'responseModel' => "defaultOutput",
            'parameters' => [
                'documento' => [
                    'location' => "uri",
                    'required' => true
                ]
            ]
        ]
    ],
    'models' => [
        'defaultOutput' => [
            'type' => "object",
            'additionalProperties' => [
                'location' => 'json'
            ]
        ]
    ]
];
?>