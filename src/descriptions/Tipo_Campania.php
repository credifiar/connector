<?php
return [
    'baseUri' => config('connector.back_uri'),
    'operations' => [
        'getAll' => [
            'summary' => "Obtiene todos los tipos de campanias que existen.",
            'httpMethod' => "GET",
            'uri' => "/campanias/tipos",
            'responseModel' => "getAllOutput"
        ]
    ],
    'models' => [
        'getAllOutput' => [
            'type' => "object",
            'additionalProperties' => [
                'location' => 'json'
            ]
        ]
    ]
];
?>