<?php
namespace Credifiar\Connector;

use GuzzleHttp\Command\Result;

class Transformer
{

    private $response;

    private $content = array();

    function __construct(Result $response)
    {
        $this->response = $response;
        $this->setContent();
    }

    private function setContent()
    {
        if (isset($this->response['response'])) {
            $this->content = $this->response['response'];
        } elseif (isset($this->response['messages']) && ! empty($this->response['messages'])) {
            $this->content = $this->response['messages'];
        } elseif (isset($this->response['exception'])) {
            throw new \Exception($this->response['exception']['title'] . " " . $this->response['exception']['detail']);
        } else {
            throw new \Exception("ERROR! No se logro obtener respuesta del back.");
        }
    }

    /**
     * Retorna los datos del response sin ninguna transformación.
     *
     * @return StdClass
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Retorna un object, si el response es un array multidimesional lo transforma en un array de objetos.
     *
     * @return array|StdClass
     */
    public function toObject()
    {
        if (is_array($this->content)) {
            foreach ($this->content as $key => $value) {
                if (is_array($value)) {
                    $this->content[$key] = (object) $value;
                } else {
                    $object = new stdClass();
                    $object->$key = $value;
                    return $object;
                }
            }
            return $this->content;
        } else {
            return new stdClass();
        }
    }

    /**
     * Devuelve una collección de laravel.
     *
     * @return Illuminate\Support\Collection
     */
    public function toCollection()
    {
        if (is_array($this->content)) {
            return collect($this->content);
        } else {
            return collect();
        }
    }
}