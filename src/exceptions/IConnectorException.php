<?php
namespace Credifiar\Connector\Exceptions;

interface IConnectorException
{

    public function toArray();

    public function toObject();
}