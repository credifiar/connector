<?php
namespace Credifiar\Connector\Exceptions;

use Credifiar\Connector\Exceptions\IConnectorException;
use GuzzleHttp\Command\Exception\CommandException;
use Psr\Http\Message\ResponseInterface;

class ConnectorException extends \Exception implements IConnectorException
{

    private $exception = array();

    function __construct(CommandException $e)
    {
        $response = $e->getResponse();
        if (! is_null($response)) {
            $content = $this->getContent($response);
            $this->exception = $this->getException($content);
        } else {
            $this->exception['detail'] = $e->getMessage();
            $this->exception['code'] = $e->getCode();
            $this->exception['file'] = $e->getFile();
            $this->exception['line'] = $e->getLine();
            $this->exception['status'] = 500;
            $this->exception['trace'] = $e->getTraceAsString();
        }
        parent::__construct($this->exception['detail'], $this->exception['code'], $e);
    }

    private function getContent(ResponseInterface $response)
    {
        $content = $response->getBody()->getContents();
        if (is_string($content)) {
            $content = json_decode($content, true);
        } else {
            throw new \Exception("Error en la excepción.");
        }
        return $content;
    }

    private function getException(array $content)
    {
        $exception = array();
        if (array_key_exists('exception', $content)) {
            $exception = $content['exception'];
        }
        return $exception;
    }

    public function toArray()
    {
        return $this->exception;
    }

    public function toObject()
    {
        $object = (object) $this->exception;
        return $object;
    }
}